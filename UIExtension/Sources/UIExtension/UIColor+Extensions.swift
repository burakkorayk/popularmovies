//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 27.11.2020.
//

import UIKit

open class UIColorExtension: UIColor {
    open class var bgColor: UIColor {
        return UIColor("#272626")
    }
    
    class var textColor: UIColor {
        return UIColor.white
    }
    
    class var primaryColor: UIColor {
        return UIColor("#6AC045")
    }
    
    class var darkColor: UIColor {
        return UIColor("#1D1C12")
    }
    
    class var cornFlowerBlue: UIColor {
        return UIColor("#4a86e8")
    }
    
    class var lightCornFlowerBlue: UIColor {
        return UIColor("#3d85c6")
    }
    
    class var lightBlue: UIColor {
        return UIColor("#1565c0")
    }
    
    class var veryLightBlue: UIColor {
        return UIColor("#1155cc")
    }
    
}

extension UIColor {
    
    convenience init(_ hex: String, alpha: CGFloat = 1.0) {
        var hexFormatted = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if hexFormatted.hasPrefix("#") {
            hexFormatted = String(hexFormatted.dropFirst())
        }
        
        assert(hexFormatted.count == 6, "Invalid hex code used.")
        
        var rgbValue: UInt64 = 0
        Scanner(string: hexFormatted).scanHexInt64(&rgbValue)
        
        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: alpha)
    }
}
