import XCTest

import UIExtensionTests

var tests = [XCTestCaseEntry]()
tests += UIExtensionTests.allTests()
XCTMain(tests)
