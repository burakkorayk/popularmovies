//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 25.11.2020.
//

import Foundation
import Service
import Entity

class PopularMovieListInteractor: PopularMovieListInteractorProtocol {
    weak var delegate: PopularMovieListInteractorOutputProtocol?
    lazy var service: MovieServiceProtocol = MovieService(baseUrl: PopularMovieListBuilder.environment.baseUrl, imageUrl: PopularMovieListBuilder.environment.imageUrl, apiKey: PopularMovieListBuilder.environment.apiKey)
    private var currentPage = 1
    private var movieList: [PopularMovieResponse.MovieModel] = []

    func fetchMovies() {
        delegate?.handle(.setLoading(true))
        
        service.getMovieList(parameters: ["page": String(currentPage)]) { [weak self] (response) in
            guard let self = self else { return }
            self.delegate?.handle(.setLoading(false))

            switch response {
            case .success(let popularMoviesResponse):
                self.movieList.append(contentsOf: popularMoviesResponse.popularMovies)
                self.delegate?.handle(.showMovieList(self.movieList))
                self.currentPage += 1
            case .failure(let error):
                self.delegate?.handle(.fail(error.localizedDescription))
            }
        }
        
    }
    
    func selectMovie(at index: Int) {
        self.delegate?.handle(.showMovieDetail(movieList[index]))
    }
}
