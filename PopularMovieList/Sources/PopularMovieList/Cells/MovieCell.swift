//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 26.11.2020.
//

import UIKit
import UIExtension
import Kingfisher

class MovieCell: UITableViewCell {
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieRateLabel: UILabel!
    @IBOutlet weak var movieOverviewLabel: UILabel!
    @IBOutlet weak var moviePosterImage: UIImageView!

    var movie: MoviePresentation? {
        didSet {
            movieTitleLabel.text = movie?.title
            movieRateLabel.text = movie?.rate
            movieOverviewLabel.text = movie?.overview
            if let posterImagePath = movie?.posterImagePath {
                moviePosterImage.kf.setImage(
                    with: URL(string: posterImagePath),
                    options: [
                        .transition(.fade(1)),
                        .cacheOriginalImage
                    ])
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColorExtension.bgColor
        selectionStyle = .none
    }
        
    override func prepareForReuse() {
        super.prepareForReuse()
        
        movieTitleLabel.text = nil
        movieRateLabel.text = nil
        movieOverviewLabel.text = nil
        moviePosterImage.image = nil
    }
}
