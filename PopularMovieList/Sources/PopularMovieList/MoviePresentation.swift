//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 25.11.2020.
//

import Foundation

struct MoviePresentation: Equatable {
    let title: String
    let imagePath: String
    let overview: String
    let rate: String
    let movieId: String
    let backdropImagePath: String
    
    public static func == (lhs: MoviePresentation, rhs: MoviePresentation) -> Bool {
        return lhs.title == rhs.title
    }
    
    init(title: String, imagePath: String, overview: String, rate: String, movieId: String, backdropImagePath: String) {
        self.title = title
        self.imagePath = imagePath
        self.overview = overview
        self.rate = rate
        self.movieId = movieId
        self.backdropImagePath = backdropImagePath
    }
    
    public var posterImagePath: String {
        if let environment = PopularMovieListBuilder.environment {
            return environment.imageUrl + imagePath
        } else {
            return ""
        }
    }
}
