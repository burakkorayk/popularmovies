//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 25.11.2020.
//

import Foundation

class PopularMovieListPresenter: PopularMovieListPresenterProtocol {
    weak var view: PopularMovieListViewProtocol?
    var interactor: PopularMovieListInteractorProtocol?
    var router: PopularMovieListRouterProtocol?
    private var movieList: [MoviePresentation] = []
    
    func loadMovieList() {
        interactor?.fetchMovies()
    }
    
    func selectMovie(for indexPath: IndexPath) {
        interactor?.selectMovie(at: indexPath.row)
    }
    
    func movie(at indexPath: IndexPath) -> MoviePresentation? {
        return movieList[indexPath.row]
    }
    
    func numberOfItems() -> Int {
        return movieList.count
    }
}

extension PopularMovieListPresenter: PopularMovieListInteractorOutputProtocol {
    func handle(_ output: PopularMovieListInteractorOutput) {
        switch output {
        case .fail(let error):
            movieList.removeAll()
            view?.handle(.fail(error))
        case .showMovieList(let movies):
            movieList = movies.map({ (movieModel) -> MoviePresentation in
                return MoviePresentation(title: movieModel.title, imagePath: movieModel.posterPath, overview: movieModel.overview, rate: String(movieModel.rate), movieId: String(movieModel.movieId), backdropImagePath: movieModel.backdropImagePath ?? "")
            })
            view?.handle(.showMovieList(movieList))
        case .setLoading(let isLoading):
            view?.handle(.setLoading(isLoading))
        case .showMovieDetail(let movieDetail):
            router?.navigate(to: .detail(MoviePresentation(title: movieDetail.title, imagePath: movieDetail.posterPath, overview: movieDetail.overview, rate: String(movieDetail.rate), movieId: String(movieDetail.movieId), backdropImagePath: movieDetail.backdropImagePath ?? "")))
        }
    }
}
