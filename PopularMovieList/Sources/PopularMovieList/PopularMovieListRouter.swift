//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 25.11.2020.
//

import Foundation
import MovieDetail

final class PopularMovieListRouter: PopularMovieListRouterProtocol {
    weak var view: PopularMovieListViewController?
    
    func navigate(to route: PopularMovieListRoute) {
        switch route {
        case .detail(let presentation):
            let moviePresentation = MovieDetailPresentation(movieId: String(presentation.movieId), title: presentation.title, posterImagePath: presentation.imagePath, overview: presentation.overview, rate: presentation.rate, backdropImagePath: presentation.backdropImagePath)
            
            let movieDetailViewController = MovieDetailBuilder.make(movie: moviePresentation, baseUrl: PopularMovieListBuilder.environment.baseUrl, imageUrl: PopularMovieListBuilder.environment.imageUrl, apiKey: PopularMovieListBuilder.environment.apiKey )
            view?.navigationController?.pushViewController(movieDetailViewController, animated: true)
        }
    }
}
