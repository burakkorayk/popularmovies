//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 25.11.2020.
//

import Foundation
import Entity

// MARK: View
protocol PopularMovieListViewProtocol: class {
    var presenter: PopularMovieListPresenterProtocol? { get set }

    func handle(_ output: PopularMovieListPresenterOutput)
}

enum PopularMovieListPresenterOutput: Equatable {
    case fail(String)
    case showMovieList([MoviePresentation])
    case setLoading(_ isLoading: Bool)
}

// MARK: Presenter
protocol PopularMovieListPresenterProtocol: class {
    var view: PopularMovieListViewProtocol? { get set }
    var interactor: PopularMovieListInteractorProtocol? { get set }
    var router: PopularMovieListRouterProtocol? { get set }

    func loadMovieList()
    func selectMovie(for indexPath: IndexPath) //-> Movie?
    func movie(at indexPath: IndexPath) -> MoviePresentation?
    func numberOfItems() -> Int
}

// MARK: Interactor
protocol PopularMovieListInteractorProtocol: class {
    var delegate: PopularMovieListInteractorOutputProtocol? { get set }

    func fetchMovies()
    func selectMovie(at index: Int)
}

protocol PopularMovieListInteractorOutputProtocol: class {
    func handle(_ output: PopularMovieListInteractorOutput)
}

enum PopularMovieListInteractorOutput {
    case fail(String)
    case showMovieList([PopularMovieResponse.MovieModel])
    case setLoading(Bool)
    case showMovieDetail(PopularMovieResponse.MovieModel)
}

// MARK: Router
enum PopularMovieListRoute: Equatable {
    case detail(_ presentation: MoviePresentation)
}

protocol PopularMovieListRouterProtocol: AnyObject {
    func navigate(to route: PopularMovieListRoute)
}
