//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 26.11.2020.
//

import UIKit

extension UITableViewCell: Reusable {

}

extension UITableView {

    func register<T: UITableViewCell>(_ cellType: T.Type, reuseIdentifier: String? = nil) {
        self.register(cellType.nib, forCellReuseIdentifier: reuseIdentifier ?? cellType.reuseIdentifier)
    }

    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath, cellType: T.Type = T.self, reuseIdentifier: String? = nil) -> T {
        let cell = self.dequeueReusableCell(withIdentifier: reuseIdentifier ?? cellType.reuseIdentifier, for: indexPath) as! T
        return cell
    }
}
