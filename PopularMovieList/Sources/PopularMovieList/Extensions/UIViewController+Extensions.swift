//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 26.11.2020.
//

import UIKit

extension UIViewController {
    private static func instanceFromNib<T: UIViewController>(name: String? = nil, bundle: Bundle? = nil) -> T {
        return T(nibName: name ?? String(describing: self), bundle: ProjectBundle.bundle())
    }

    public static func instantiate(name: String? = nil, bundle: Bundle? = nil) -> Self {
        return instanceFromNib(name: name, bundle: ProjectBundle.bundle())
    }
}

open class ProjectBundle {
    class func bundle() -> Bundle {
        let podBundle = Bundle.module
        if let url = podBundle.url(forResource: "PopularMovieList", withExtension: "bundle") {
            let bundle = Bundle(url: url)
            return bundle ?? podBundle
        }
        return podBundle
    }
}

