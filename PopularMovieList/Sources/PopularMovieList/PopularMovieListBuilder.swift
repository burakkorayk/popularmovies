//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 25.11.2020.
//

import UIKit

public struct Environment {
    let baseUrl: String
    var imageUrl: String
    let apiKey: String
}

public final class PopularMovieListBuilder {
    static var environment: Environment!
    public static func make(baseUrl: String, imageUrl: String, apiKey: String) -> PopularMovieListViewController {
        let viewController = PopularMovieListViewController.instantiate()
        let interactor = PopularMovieListInteractor()
        let presenter = PopularMovieListPresenter()
        let router = PopularMovieListRouter()
        environment = Environment(baseUrl: baseUrl, imageUrl: imageUrl, apiKey: apiKey)

        router.view = viewController
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        interactor.delegate = presenter
        
        return viewController
    }
}

