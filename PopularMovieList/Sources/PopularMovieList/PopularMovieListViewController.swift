//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 25.11.2020.
//

import UIKit
import UIExtension
import Extensions

private enum Constans {
    static let rowHeight: CGFloat = 237
}

open class PopularMovieListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var searchActive : Bool = false
    
    lazy var searchBar = UISearchBar(frame: CGRect.zero)
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        //        refreshControl.addTarget(self, action:
        //                     #selector(handleRefresh(_:)),
        //                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    
    var presenter: PopularMovieListPresenterProtocol?
    private var initalMovieList: [MoviePresentation] = []
    private var movieList: [MoviePresentation] = []
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        addSearchBar()
    }
    
    private func setupView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColorExtension.bgColor
        tableView.register(UINib.init(nibName: "MovieCell", bundle: ProjectBundle.bundle()), forCellReuseIdentifier: "MovieCell")
        tableView.addSubview(self.refreshControl)
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.loadMovieList()
    }
}

extension PopularMovieListViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constans.rowHeight
    }
}

extension PopularMovieListViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieList.count
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let movieCell = tableView.dequeueReusableCell(for: indexPath, cellType: MovieCell.self)
        movieCell.movie = movieList[indexPath.row]
        return movieCell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.selectMovie(for: indexPath)
    }
    
    // TODO: loadmore
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = movieList.count - 1
        if indexPath.row == lastElement && !searchActive {
            presenter?.loadMovieList()
        }
    }
}

extension PopularMovieListViewController: PopularMovieListViewProtocol {
    func handle(_ output: PopularMovieListPresenterOutput) {
        switch output {
        case .setLoading(let isLoading):
            if isLoading {
                PopularMovieListViewController.showSpinner(onView: self.view)
            } else {
                PopularMovieListViewController.removeSpinner()
            }
        case .fail(let error):
            DispatchQueue.main.async { [weak self] in
                let alert = UIAlertController.customAlertController(title: "Error!", message: error)
                self?.present(alert, animated: true) {
                    // TODO : reload
                }
            }
        case .showMovieList(let movieList):
            self.initalMovieList.append(contentsOf: movieList)
            self.movieList.append(contentsOf: movieList)
            DispatchQueue.main.async { [weak self] in
                PopularMovieListViewController.removeSpinner()
                self?.tableView.reloadData()
            }
        }
    }
}

extension PopularMovieListViewController {
    private func addSearchBar() {
        searchBar.sizeToFit()
        searchBar.delegate = self
        self.searchBar.showsCancelButton = false
        navigationItem.titleView = searchBar
    }
}

extension PopularMovieListViewController: UISearchBarDelegate {
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.resignFirstResponder()
        self.searchBar.showsCancelButton = false
        searchActive = false;
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchBar.showsCancelButton = true
        let searchBarText = searchBar.text ?? ""
        
        self.movieList = initalMovieList
        
        self.movieList = movieList.filter { (movie) -> Bool in
            return movie.title.contains(searchBarText)
        }
        
        if searchBarText == "" {
            searchActive = false
            self.movieList = initalMovieList
        }
        
        if(self.movieList.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tableView.reloadData()
    }
}
