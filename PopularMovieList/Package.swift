// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "PopularMovieList",
    platforms: [.iOS(.v10)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "PopularMovieList",
            targets: ["PopularMovieList"]),
    ],
    dependencies: [
        .package(path: "../Service"),
        .package(path: "../Extensions"),
        .package(path: "../MovieDetail"),
        .package(url: "https://github.com/onevcat/Kingfisher.git", from: "5.15.7"),
        .package(url: "https://github.com/Quick/Quick.git", .upToNextMajor(from: "2.0.0")),
        .package(url: "https://github.com/Quick/Nimble.git", .upToNextMajor(from: "8.0.1"))
    ],
    targets: [
        .target(
            name: "PopularMovieList",
            dependencies: [.product(name: "Service", package: "Service"),.product(name: "Extensions", package: "Extensions"),.product(name: "MovieDetail", package: "MovieDetail"),.byName(name: "Kingfisher")],
            resources: [
                .copy("PopularMovieListViewController.xib"),
                .copy("Cells/MovieCell.xib"),
                ]),
        .testTarget(
            name: "PopularMovieListTests",
            dependencies: ["PopularMovieList","Quick","Nimble"]),
    ]
)
