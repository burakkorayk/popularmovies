//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 28.11.2020.
//

import XCTest
import Service
import Entity
import Network
import Nimble
import Quick

@testable import PopularMovieList

class MovieListInteractorTests: QuickSpec {
    
    override func spec() {
        describe("PopularMovieListInteractor") {
            
            var interactor: PopularMovieListInteractor!
            var service: MockMovieService!
            var delegate: MockMovieListInteractorOutput!
            
            beforeEach {
                interactor = PopularMovieListInteractor()
                service = MockMovieService()
                delegate = MockMovieListInteractorOutput()
                interactor.service = service
                interactor.delegate = delegate
            }
            
            describe("#fetchMovies") {
                
                context("the movies were fetched successfully") {
                    it("return the movie list to the output") {
                        
                        let movieList = try? ResourceLoader.loadMovieList(from: ResourceLoader.JsonFileName.movieListResponseCorrect.rawValue)
                        service.mockPopularMoviesResult = .success(movieList ?? PopularMovieResponse())
                                                
                        interactor.fetchMovies()
                        
                        expect(delegate.didCallMoviesDidFetch).to(beTrue())
                        expect(delegate.didCallMoviesDidFetchWithMovies).to(equal(movieList?.popularMovies))
                        expect(service.didCallFetchMovies).to(beTrue())
                    }
                }
                
                context("the movies were not fetched successfully") {
                    it("return the error to the output") {
                        let error = NetworkError.failed
                        service.mockPopularMoviesResult = .failure(error)
                        interactor.fetchMovies()
                        
                        expect(delegate.didCallMoviesFailToFetch).to(beTrue())
                        expect(delegate.didCallMoviesFailToFetchWithError).to(equal(error.localizedDescription))
                        expect(service.didCallFetchMovies).to(beTrue())
                    }
                }
            }
        }
    }
}

