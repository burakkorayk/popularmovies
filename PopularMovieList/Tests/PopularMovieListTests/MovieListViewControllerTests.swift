//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 28.11.2020.
//

import XCTest
import Quick
import Nimble

@testable import PopularMovieList

class MovieListViewControllerTests: QuickSpec {
    override func spec() {
        describe("MovieListViewController") {
            var viewController : PopularMovieListViewController!
            var presenter : MockMovieListPresenter!
            
            beforeEach {
                viewController = PopularMovieListViewController.instantiate()
                viewController.loadView()
                presenter = MockMovieListPresenter()
                viewController.presenter = presenter
            }
            
            describe("#init") {
                it("sets a tableview") {
                    expect(viewController.tableView).toNot(beNil())
                }
            }
            
            describe("#viewDidLoad") {
                beforeEach {
                    viewController.viewDidLoad()
                }
                
                it("calls the presenter's viewDidLoad") {
                    viewController.viewWillAppear(false)
                    expect(presenter.didCallViewWillAppear).to(beTrue())
                }
            }
            
            describe(".tableView") {
                it("delegate is MovieListViewController") {
                    
                    expect(viewController.tableView.delegate).to(be(viewController))
                }

                it("dataSource is MovieListViewController") {
                    
                    expect(viewController.tableView.dataSource).to(be(viewController))
                }
            }
        }
    }
}
