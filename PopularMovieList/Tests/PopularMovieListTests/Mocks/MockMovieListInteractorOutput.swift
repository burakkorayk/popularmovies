//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 28.11.2020.
//

@testable import PopularMovieList
import Service
import Entity

class MockMovieListInteractorOutput: PopularMovieListInteractorOutputProtocol {
    var didCallMoviesDidFetch = false
    var didCallMoviesDidFetchWithMovies: [PopularMovieResponse.MovieModel] = []
    
    var didCallMoviesFailToFetch = false
    var didCallMoviesFailToFetchWithError: String?

    func handle(_ output: PopularMovieListInteractorOutput) {
        switch output {
        case .showMovieList(let movies):
            didCallMoviesDidFetch = true
            didCallMoviesDidFetchWithMovies = movies
        case .fail(let reason):
            didCallMoviesFailToFetch = true
            didCallMoviesFailToFetchWithError = reason
        case .setLoading(let isLoading):
            break
        case .showMovieDetail(let movie):
            break
        }
    }
}
