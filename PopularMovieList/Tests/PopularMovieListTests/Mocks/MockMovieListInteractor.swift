//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 28.11.2020.
//

import Foundation

@testable import PopularMovieList

class MockMovieListInteractor: PopularMovieListInteractorProtocol {
    var didCallFetchMovies = false
    var delegate: PopularMovieListInteractorOutputProtocol?
    
    func fetchMovies() {
        didCallFetchMovies = true
    }
    
    func selectMovie(at index: Int) {
        //
    }
}

