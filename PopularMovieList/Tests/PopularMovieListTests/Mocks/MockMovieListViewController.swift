//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 28.11.2020.
//

import XCTest
@testable import PopularMovieList

class MockMovieListViewController: PopularMovieListViewProtocol {
    var presenter: PopularMovieListPresenterProtocol?
    
    var didCallloadData = false
    var didCallShowError = false
    var didCallShowErrorWith = ""

    func handle(_ output: PopularMovieListPresenterOutput) {
        switch output {
        case .fail(let reason):
            didCallShowError = true
            didCallShowErrorWith = reason
        case .setLoading(let isLoading):
            break
        case .showMovieList(let movies):
            didCallloadData = true
        }
    }
}
