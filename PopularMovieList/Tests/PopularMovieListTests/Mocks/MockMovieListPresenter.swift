//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 28.11.2020.
//

@testable import PopularMovieList
import UIKit

class MockMovieListPresenter: PopularMovieListPresenterProtocol {
    
    var view: PopularMovieListViewProtocol?
    var interactor: PopularMovieListInteractorProtocol?
    var router: PopularMovieListRouterProtocol?
    
    var didCallViewWillAppear = false
    var movieToReturn: MoviePresentation?
    var numberOfItemsToReturn = 0
    var didCallMovieForIndexPathWith: IndexPath!

    func loadMovieList() {
        didCallViewWillAppear = true
    }
    
    func selectMovie(for indexPath: IndexPath) {
        
    }
    
    func movie(at indexPath: IndexPath) -> MoviePresentation? {
        didCallMovieForIndexPathWith = indexPath
        return movieToReturn
    }
    
    func numberOfItems() -> Int {
        return numberOfItemsToReturn
    }
}
