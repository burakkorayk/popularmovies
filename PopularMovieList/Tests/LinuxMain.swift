import XCTest

import PopularMovieListTests

var tests = [XCTestCaseEntry]()
tests += PopularMovieListTests.allTests()
XCTMain(tests)
