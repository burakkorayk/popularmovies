//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 26.11.2020.
//

import Foundation
import Log

class NetworkLogger {
    static func log(request: URLRequest) {
        Logger.log(.info(""))
        Logger.log(.info("\n----------OUTGOING----------\n"))
        defer { Logger.log(.info("\n----------END----------\n"))}
        
        let urlAsString = request.url?.absoluteString ?? ""
        let urlComponents = NSURLComponents(string: urlAsString)
        
        let method = request.httpMethod != nil ? "\(request.httpMethod ?? "")" : ""
        let path = "\(urlComponents?.path ?? "")"
        let query = "\(urlComponents?.query ?? "")"
        let host = "\(urlComponents?.host ?? "")"
        
        var logOutput = """
        \(urlAsString) \n\n
        \(method) \(path)?\(query) HTTP/1.1 \n
        HOST: \(host)\n
        """
        for (key, value) in request.allHTTPHeaderFields ?? [:] {
            logOutput += "\(key): \(value) \n"
        }
        if let body = request.httpBody {
            logOutput += "\n \(NSString(data: body, encoding: String.Encoding.utf8.rawValue) ?? "")"
        }
        
        Logger.log(.info(logOutput))
    }
    
    static func log(response: URLResponse) {}
}
