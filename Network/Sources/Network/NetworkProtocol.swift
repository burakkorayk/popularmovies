//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 26.11.2020.
//

import Foundation

public protocol NetworkProtocol {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}
