//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 26.11.2020.
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}
