//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 27.11.2020.
//

import Foundation

extension Array {
    public subscript(safeIndex index: Int) -> Element? {
        guard index >= 0, index < endIndex else {
            return nil
        }
        
        return self[index]
    }
}
