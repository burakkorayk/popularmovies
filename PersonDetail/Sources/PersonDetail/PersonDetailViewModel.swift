//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 28.11.2020.
//

import Foundation
import Entity

class PersonDetailViewModel: PersonDetailViewModelProtocol {
    
    weak var delegate: PersonDetailViewModelDelegate?
    var cast:MovieCreditResponse.Cast!
        
    func loadPersonDetail() {
        delegate?.handleViewModelOutput(.setLoading(true))
        
        delegate?.handleViewModelOutput(.showPersonDetail(cast))
        delegate?.handleViewModelOutput(.setLoading(false))
    }
}
