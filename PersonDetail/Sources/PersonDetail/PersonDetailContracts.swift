//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 28.11.2020.
//

import Foundation
import Entity

protocol PersonDetailViewModelProtocol {
    var delegate: PersonDetailViewModelDelegate? { get set }
    func loadPersonDetail()
}

enum PersonDetailViewModelOutput :Equatable {
    case setLoading(Bool)
    case showPersonDetail(MovieCreditResponse.Cast)
    case showError(String)
}

enum PersonDetailViewRoute {
    
}

protocol PersonDetailViewModelDelegate: class {
    func handleViewModelOutput(_ output: PersonDetailViewModelOutput)
}
