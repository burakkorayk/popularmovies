//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 28.11.2020.
//

import Foundation
import Entity
import UIKit

public final class PersonDetailBuilder {

    public static func make(cast: MovieCreditResponse.Cast) -> PersonDetailViewController {
        let vc = PersonDetailViewController.instantiate()
        let viewmodel = PersonDetailViewModel()
        viewmodel.cast = cast
        vc.viewModel = viewmodel
        
        return vc
    }
    
}

extension UIViewController {
    private static func instanceFromNib<T: UIViewController>(name: String? = nil, bundle: Bundle? = nil) -> T {
        return T(nibName: name ?? String(describing: self), bundle: ProjectBundle.bundle())
    }

    public static func instantiate(name: String? = nil, bundle: Bundle? = nil) -> Self {
        return instanceFromNib(name: name, bundle: ProjectBundle.bundle())
    }
}

open class ProjectBundle {
    class func bundle() -> Bundle {
        let podBundle = Bundle.module
        if let url = podBundle.url(forResource: "PersonDetail", withExtension: "bundle") {
            let bundle = Bundle(url: url)
            return bundle ?? podBundle
        }
        return podBundle
    }
}
