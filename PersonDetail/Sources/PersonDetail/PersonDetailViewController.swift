//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 28.11.2020.
//

import UIKit
import Kingfisher

open class PersonDetailViewController: UIViewController {

    @IBOutlet weak var personImageView: UIImageView!
    @IBOutlet weak var personNameLabel: UILabel!
    @IBOutlet weak var characterNameLabel: UILabel!

    var viewModel: PersonDetailViewModelProtocol! {
        didSet {
            viewModel.delegate = self
        }
    }
    
    open override func viewDidLoad() {
        viewModel.loadPersonDetail()
        view.backgroundColor = UIColor.black
    }
    
}

extension PersonDetailViewController: PersonDetailViewModelDelegate {
    func handleViewModelOutput(_ output: PersonDetailViewModelOutput) {
        switch output {
        case .setLoading(let isLoading):
            break
        case .showError(let error):
            break
        case .showPersonDetail(let cast):
            personNameLabel.text = cast.name
            characterNameLabel.text = cast.character
            personImageView.kf.setImage(
                with: URL(string: cast.getProfileImageFullPath),
                options: [
                    .transition(.fade(1)),
                    .cacheOriginalImage
                ])
        break
        }
    }
}
