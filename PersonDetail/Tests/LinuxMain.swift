import XCTest

import PersonDetailTests

var tests = [XCTestCaseEntry]()
tests += PersonDetailTests.allTests()
XCTMain(tests)
