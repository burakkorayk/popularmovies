import XCTest
@testable import PersonDetail

final class PersonDetailTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(PersonDetail().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
