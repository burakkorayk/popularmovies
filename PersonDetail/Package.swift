// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "PersonDetail",
    platforms: [.iOS(.v10)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "PersonDetail",
            targets: ["PersonDetail"]),
    ],
    dependencies: [
        .package(path: "../Entity"),
        .package(url: "https://github.com/onevcat/Kingfisher.git", from: "5.15.7")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "PersonDetail",
            dependencies: [.product(name: "Entity", package: "Entity"),.byName(name: "Kingfisher")]),
        .testTarget(
            name: "PersonDetailTests",
            dependencies: ["PersonDetail"]),
    ]
)
