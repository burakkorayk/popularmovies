//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 27.11.2020.
//

import Foundation
import RxSwift
import Entity
import Service

enum ViewState {
    case loading
    case success
    case failure
}

class MovieDetailViewModel {
    lazy var service: MovieServiceProtocol = MovieService(baseUrl: MovieDetailBuilder.environment.baseUrl, imageUrl: MovieDetailBuilder.environment.imageUrl, apiKey: MovieDetailBuilder.environment.apiKey)
    let fetchPublisher = BehaviorSubject<[MovieCreditResponse.Cast]?>(value: nil)
    let statePublisher = BehaviorSubject<ViewState>(value: .loading)
    var disposeBag: DisposeBag!
    
    init() {
        disposeBag = DisposeBag()
    }
    
    func getDetail(movieId: String) {
        self.statePublisher.onNext(.loading)
        service.getMovieCredits(movieId: movieId) { [weak self] (response) in
            guard let self = self else { return }
            switch response {
            case .success(let movieCredits):
                self.fetchPublisher.onNext(movieCredits.cast)
                self.statePublisher.onNext(.success)
            case .failure(let error):
                self.statePublisher.onNext(.failure)
                self.fetchPublisher.onError(error)
            }
        }
    }
}
