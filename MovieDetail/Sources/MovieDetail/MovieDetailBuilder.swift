//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 27.11.2020.
//

import UIKit

public struct Environment {
    let baseUrl: String
    let imageUrl: String
    let apiKey: String
}


public final class MovieDetailBuilder {
    static var environment: Environment!

    public static func make(movie: MovieDetailPresentation, baseUrl: String, imageUrl: String, apiKey: String) -> MovieDetailViewController {
        environment = Environment(baseUrl: baseUrl, imageUrl: imageUrl, apiKey: apiKey)
        let viewController = MovieDetailViewController.instantiate()
        viewController.movieDetail = movie
        let viewModel = MovieDetailViewModel()
        viewController.viewModel = viewModel
        
        return viewController
    }
}

