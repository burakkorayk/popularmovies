//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 28.11.2020.
//

import UIKit

class CastCell: UICollectionViewCell  {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var profileImagePath: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImageView.image = nil
        profileImageView.layer.borderWidth = 1.0
        profileImageView.layer.masksToBounds = false
        profileImageView.layer.borderColor = UIColor.white.cgColor
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        profileImageView.clipsToBounds = true
    }
}
