//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 27.11.2020.
//

import Foundation

open class MovieDetailPresentation: Equatable {
    let movieId: String
    let title: String
    let posterImagePath: String
    let backdropImagePath: String
    let overview: String
    let rate: String
    
    public static func == (lhs: MovieDetailPresentation, rhs: MovieDetailPresentation) -> Bool {
        return lhs.title == rhs.title
    }
    
    public init(movieId: String,title: String, posterImagePath: String, overview: String, rate: String, backdropImagePath: String) {
        self.movieId = movieId
        self.title = title
        self.posterImagePath = posterImagePath
        self.overview = overview
        self.rate = rate
        self.backdropImagePath = backdropImagePath
    }
    
    public var getBackdropImageFullPath: String {
        return MovieDetailBuilder.environment.imageUrl + backdropImagePath
    }
    
    public var getPosterImageFullPath: String {
        return MovieDetailBuilder.environment.imageUrl + posterImagePath
    }
    
}
