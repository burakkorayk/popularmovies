//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 27.11.2020.
//

import UIKit
import UIExtension
import RxSwift
import Entity
import Kingfisher
import PersonDetail

open class MovieDetailViewController: UIViewController {
    
    @IBOutlet private weak var castCollectionView: UICollectionView!
    @IBOutlet private weak var backdropImageView: UIImageView!
    @IBOutlet private weak var posterImageView: UIImageView!
    @IBOutlet private weak var ratingLabel: UILabel!
    @IBOutlet private weak var seasonInfoLabel: UILabel!
    @IBOutlet private weak var overviewLabel: UILabel!
    @IBOutlet private weak var genrelabel: UILabel!
    
    var viewModel: MovieDetailViewModel!
    var disposeBag: DisposeBag!
    var movieDetail: MovieDetailPresentation!
    var cast: [MovieCreditResponse.Cast] = []
    
    open override func viewDidLoad() {
        disposeBag = DisposeBag()
        bindings()
     
        viewModel.getDetail(movieId: movieDetail.movieId)
        
        viewModel.statePublisher.asObservable().subscribe(onNext: { [weak self] (state) in
            guard let self = self else {return}
            switch state {
            case .loading:
                MovieDetailViewController.showSpinner(onView: self.view)
                break
            case .failure:
                DispatchQueue.main.async {
                    MovieDetailViewController.removeSpinner()
                }
                break
            case .success:
                self.cast = try! self.viewModel.fetchPublisher.value()!
                DispatchQueue.main.async {
                    self.castCollectionView.reloadData()
                    MovieDetailViewController.removeSpinner()
                }
            }
        }).disposed(by: disposeBag)
    }
    
    private func bindings() {
        title = movieDetail.title
        view.backgroundColor = UIColor.black
        
        ratingLabel.text = movieDetail.rate
        //seasonInfoLabel.text = movieDetail.
        overviewLabel.text = movieDetail.overview
        posterImageView.kf.setImage(
            with: URL(string: movieDetail.getPosterImageFullPath),
            options: [
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        backdropImageView.kf.setImage(
            with: URL(string: movieDetail.getBackdropImageFullPath),
            options: [
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        castCollectionView.delegate = self
        castCollectionView.dataSource = self
        castCollectionView.register(UINib.init(nibName: "CastCell", bundle: ProjectBundle.bundle()), forCellWithReuseIdentifier: "CastCell")
        
    }
}

extension MovieDetailViewController: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cast.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CastCell",
                                                            for: indexPath) as? CastCell else {
            fatalError("Cell is not found")
        }
        
        let castItem = cast[indexPath.row]
        cell.profileImageView.kf.setImage(
            with: URL(string: castItem.getProfileImageFullPath),
            options: [
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        
        cell.nameLabel.text = castItem.name
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        present(PersonDetailBuilder.make(cast: cast[indexPath.row]), animated: true)
    }
}

extension MovieDetailViewController: UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                               insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                               minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return .init(width: 80, height: 80)
    }
}
