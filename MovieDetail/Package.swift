// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MovieDetail",
    platforms: [.iOS(.v10)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "MovieDetail",
            targets: ["MovieDetail"]),
    ],
    dependencies: [
        .package(path: "../Service"),
        .package(path: "../UIExtension"),
        .package(path: "../PersonDetail"),
        .package(url: "https://github.com/ReactiveX/RxSwift.git", from: "5.1.1"),
        .package(url: "https://github.com/onevcat/Kingfisher.git", from: "5.15.7")

    ],
    targets: [
        .target(
            name: "MovieDetail",
            dependencies: [.product(name: "Service", package: "Service"),.product(name: "UIExtension", package: "UIExtension"),.byName(name: "RxSwift"),.byName(name: "Kingfisher"),
                           .product(name: "PersonDetail", package: "PersonDetail")]),
        .testTarget(
            name: "MovieDetailTests",
            dependencies: ["MovieDetail"]),
    ]
)
