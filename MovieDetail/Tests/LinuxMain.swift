import XCTest

import MovieDetailTests

var tests = [XCTestCaseEntry]()
tests += MovieDetailTests.allTests()
XCTMain(tests)
