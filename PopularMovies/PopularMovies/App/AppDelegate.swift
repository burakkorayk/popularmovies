//
//  AppDelegate.swift
//  PopularMovies
//
//  Created by Burak Koray Kose on 25.11.2020.
//

import UIKit
import PopularMovieList

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        AppConfig.configure()
        initMainScreen()

        return true
    }
    
    private func initMainScreen() {
        window = UIWindow(frame: UIScreen.main.bounds)

        let popularMovieListViewController = PopularMovieListBuilder.make(baseUrl: AppConfig.environment.baseUrl, imageUrl: AppConfig.environment.imageUrl, apiKey: AppConfig.environment.apiKey)

        let navigationController = UINavigationController(rootViewController: popularMovieListViewController)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

