//
//  AppConfig.swift
//  PopularMovies
//
//  Created by Burak Koray Kose on 25.11.2020.
//

import Foundation
import Log

final class AppConfig {
    static var environment: Environment!
    static var isDebug: Bool = false
    
    static func configure() {
        #if DEBUG
        isDebug = true
        #endif
        
        loadFromConfigFile()
    }
    
    private static func loadFromConfigFile() {
        
        guard let path = Bundle.main.path(forResource: "Info", ofType: "plist"), let config = NSDictionary(contentsOfFile: path) as? [String: Any] else {
            fatalError("No found plist file!")
        }
        
        environment = Environment(baseUrl: config["BaseUrl"] as! String,apiKey: config["ApiKey"] as! String, imageUrl: config["ImageUrl"] as! String)
        Logger.log(.info("Application has been loaded with info.plist successfully."))
        Logger.log(.info("API base url: \(environment.baseUrl)"))
    }
}
