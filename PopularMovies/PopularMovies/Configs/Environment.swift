//
//  Environment.swift
//  PopularMovies
//
//  Created by Burak Koray Kose on 25.11.2020.
//

import Foundation

struct Environment {
    let baseUrl: String
    let apiKey: String
    let imageUrl: String
}
