import XCTest

import EntityTests

var tests = [XCTestCaseEntry]()
tests += EntityTests.allTests()
XCTMain(tests)
