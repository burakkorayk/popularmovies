import XCTest
@testable import Entity

final class EntityTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(Entity().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
