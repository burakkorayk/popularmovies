//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 28.11.2020.
//

import Foundation

public struct MovieCreditResponse: Decodable {
    public let cast: [Cast]
    
    public init() {
        cast = []
    }
    
    // MARK: Cast
    public struct Cast: Decodable, Equatable {
        public let gender: Int?
        public let name, character, profilePath: String?
        public let popularity: Double?
        enum CodingKeys: String, CodingKey {
            case gender
            case name
            case character
            case popularity
            case profilePath = "profile_path"
        }
        
        public init(gender: Int, name: String, character: String, profilePath: String, popularity: Double) {
            self.gender = gender
            self.name = name
            self.character = character
            self.profilePath = profilePath
            self.popularity = popularity
        }
        
        public var getProfileImageFullPath: String {
            if let profileImagPath = profilePath {
                return "https://image.tmdb.org/t/p/w500/" + profileImagPath
            } else {
                return ""
            }
        }
    }
}
