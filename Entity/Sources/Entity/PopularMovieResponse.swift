//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 25.11.2020.
//

import Foundation

// MARK: MovieModel
public struct PopularMovieResponse: Decodable {
    let page, totalResults, totalPages: Int
    public let popularMovies: [MovieModel]

    public init() {
        page = 0
        totalPages = 0
        totalResults = 0
        popularMovies = []
    }
    
    enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case popularMovies = "results"
    }

    // MARK: MovieModel
    public struct MovieModel: Decodable, Equatable {
        public let posterPath: String
        public let title: String
        public let overview, releaseDate: String
        public let rate: Double
        public let movieId: Int
        public let backdropImagePath: String?
        enum CodingKeys: String, CodingKey {
            case posterPath = "poster_path"
            case title
            case overview
            case movieId = "id"
            case rate = "vote_average"
            case releaseDate = "release_date"
            case backdropImagePath = "backdrop_path"
        }
        
        public init(posterPath: String, title: String, overview: String, releaseDate: String, rate: Double, movieId: Int, backdropImagePath: String) {
            self.posterPath = posterPath
            self.title = title
            self.overview = overview
            self.rate = rate
            self.releaseDate = releaseDate
            self.movieId = movieId
            self.backdropImagePath = backdropImagePath
        }
    }
}
