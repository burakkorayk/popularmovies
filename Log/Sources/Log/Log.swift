
import Foundation

public enum LogType {
    case error(String)
    case errorObject(Error)
    case info(String)
    case infoObject(Any)
    
    var prefix: String {
        switch self {
        case .error:
          return "⛔️APP-ERROR: "
        case .errorObject:
            return "⛔️APP-ERROR-TRACE: "
        case .info:
          return "⚠️APP-INFO: "
        case .infoObject:
          return "⚠️APP-INFO-TRACE: "
        }
    }
}

public enum Logger {
   public static func log(_ logType: LogType) {
        switch logType {
        case .error(let message):
            print("\(logType.prefix) \(message)")
        case .errorObject(let error):
            print("\(logType.prefix) \(error)")
        case .info(let message):
            print("\(logType.prefix) \(message)")
        case .infoObject(let object):
            print("\(logType.prefix) \(object)")
        }
    }
}

/// Override Swift.print declecation
/// - Parameters:
///   - items: Any array of objects
///   - separator: Default is `blank`
///   - terminator: Defaults is `\n`
public func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    #if DEBUG
    var idx = items.startIndex
    let endIdx = items.endIndex

    repeat {
        Swift.print(items[idx], separator: separator, terminator: idx == (endIdx - 1) ? terminator : separator)
        idx += 1
    } while idx < endIdx

    #else
    var idx = items.startIndex
    let endIdx = items.endIndex

    repeat {
        Swift.print(items[idx], separator: separator, terminator: idx == (endIdx - 1) ? terminator : separator)
        idx += 1
    } while idx < endIdx

    
    #endif
}
