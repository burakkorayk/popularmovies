//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 26.11.2020.
//

import Foundation
import Network

public enum MovieAPI {
    case movies(page: Int)
    case credits(movieId: String)
}

extension MovieAPI: NetworkProtocol {
    public var baseURL: URL {
        return URL(string: serviceBaseUrl.removingPercentEncoding ?? "")!
    }
    
    public var path: String {
        switch self {
        case .movies(let page):
            return "movie/popular"
        case .credits(let movieId):
            return "movie/" + movieId + "/credits"
        }
    }
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .movies:
            return .get
        case .credits(_):
            return .get
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .movies(let page):
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: ["api_key": serviceApiKey, "page": page])
        case .credits(_):
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: ["api_key": serviceApiKey])
        }
    }
    
    public var headers: HTTPHeaders? {
        return nil
    }
}
