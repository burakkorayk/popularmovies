//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 26.11.2020.
//

import Foundation
import Network
import Entity

enum Constants {
    // MARK: Strings
                                                //2020-03-23T16:00:00+00:00
    static let dateLongMappingFormat: String = "yyyy-MM-dd'T'HH:mm:ssZ"
    static let dateShortMappingFormat: String = "yyyy-MM-dd HH:mm"
}

extension DateFormatter {
    convenience init (format: String) {
        self.init()

        dateFormat = format
        locale = Locale.current
    }
    
    static let apiDateFormat: DateFormatter = {
        let formatter = DateFormatter(format: Constants.dateLongMappingFormat)
        return formatter
    }()
}


public var decoder: JSONDecoder {
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .formatted(.apiDateFormat)
    return decoder
}

public protocol MovieServiceProtocol {
    func getMovieList(parameters:[String: String], completion: @escaping (Result<PopularMovieResponse,NetworkError>) -> Void)
    func getMovieCredits(movieId:String, completion: @escaping (Result<MovieCreditResponse,NetworkError>) -> Void)
}

public var serviceBaseUrl: String = ""
public var imageBaseUrl: String = ""
public var serviceApiKey: String = ""

public class MovieService: MovieServiceProtocol {

    public var baseUrl: String = ""
    public var imageUrl: String = ""
    public var apiKey: String = ""
    
    public init(
        baseUrl: String, imageUrl: String, apiKey: String) {
        serviceBaseUrl = baseUrl
        imageBaseUrl = imageUrl
        serviceApiKey = apiKey
    }
    
    let router = Router<MovieAPI>()
       
    public func getMovieList(parameters: [String: String], completion: @escaping (Result<PopularMovieResponse,NetworkError>) -> Void) {
        router.request(.movies(page: Int(parameters["page"] ?? "0") ?? 0)) { (data, response, error) in
            if error != nil {
                completion(.failure(NetworkError.connectionError))
            }
            
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(.failure(NetworkError.noData))
                        return
                    }
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                        
                        let apiResponse = try decoder.decode(PopularMovieResponse.self, from: responseData)
                        
                        completion(.success(apiResponse))
                    } catch {
                        print(error)
                        completion(.failure(NetworkError.unableToDecode))
                    }
                case .failure(let failure):
                    completion(.failure(NetworkError.failed))
                }
            }
        }
    }
    
    public func getMovieCredits(movieId: String, completion: @escaping (Result<MovieCreditResponse,NetworkError>) -> Void) {
        router.request(.credits(movieId: movieId)) { (data, response, error) in
            if error != nil {
                completion(.failure(NetworkError.connectionError))
            }
            
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(.failure(NetworkError.noData))
                        return
                    }
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                        
                        let apiResponse = try decoder.decode(MovieCreditResponse.self, from: responseData)
                        
                        completion(.success(apiResponse))
                    } catch {
                        print(error)
                        completion(.failure(NetworkError.unableToDecode))
                    }
                case .failure(let failure):
                    completion(.failure(NetworkError.failed))
                }
            }
        }
    }
    
    
    fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<Bool,NetworkError> {
        switch response.statusCode {
        case 200...299: return .success(true)
        case 401...500: return .failure(NetworkError.authenticationError)
        case 501...599: return .failure(NetworkError.badRequest)
        case 600: return .failure(NetworkError.outdated)
        default: return .failure(NetworkError.failed)
        }
    }
}
