//
//  File.swift
//  
//
//  Created by Burak Koray Kose on 28.11.2020.
//

import Foundation
import Network
import Entity

public final class MockMovieService: MovieServiceProtocol {
    public var didCallFetchMovies = false

    public init() {}
    
    public var mockPopularMoviesResult: Result<PopularMovieResponse, NetworkError>?
    
    public func getMovieList(parameters: [String : String], completion: @escaping (Result<PopularMovieResponse, NetworkError>) -> Void) {
        didCallFetchMovies = true
        guard let mockResult = mockPopularMoviesResult else { return }
        completion(mockResult)
    }
    
    public func getMovieCredits(movieId: String, completion: @escaping (Result<MovieCreditResponse, NetworkError>) -> Void) {
        
    }
}
