// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Service",
    platforms: [.iOS(.v10)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "Service",
            targets: ["Service"]),
    ],
    dependencies: [
        .package(path: "../Network"),
        .package(path: "../Entity")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "Service",
            dependencies: [.product(name: "Network", package: "Network"), .product(name: "Entity", package: "Entity")],
            resources: [.copy("MovisList.json"),.copy("MovieList_Empty.json"), .copy("MovieList_Incorrectdata.json")]),
        .testTarget(
            name: "ServiceTests",
            dependencies: ["Service"],
            resources: [.copy("MovieList.json"),.copy("MovieList_Empty.json"), .copy("MovieList_Incorrectdata.json")]),
    ]
)
