import XCTest
import Entity
@testable import Service

public enum JsonFileName: String {
    case movieListResponseCorrect = "MovieList"
    case movieListResponseEmpty = "MovieList_Empty"
    case movieListResponseIncorrect = "MovieList_Incorrectdata"
}

final class ServiceTests: XCTestCase {

    var networkManager: MockMovieService!
    
    override func setUp() {
        networkManager = MockMovieService()
    }
    
    override func tearDown() {
        networkManager = nil
    }
    
    func testMockNetworkManagerWithCorrectData() {
        let popularMovieList: PopularMovieResponse = try! ResourceLoader.loadMovieList(from: JsonFileName.movieListResponseCorrect.rawValue)
        networkManager.mockPopularMoviesResult = .success(popularMovieList)
        
        networkManager.getMovieList(parameters: ["page": "1"]) { (response) in
            switch response {
            case .success(let movies):
                XCTAssert(!movies.popularMovies.isEmpty, "The API service doesn't response correct data back.")
            case .failure(let error):
                XCTAssert(error == nil, "Network request got error.")
            }
        }
    }
    
    func testMockNetworkManagerWithEmptyData() {
        let popularMovieList: PopularMovieResponse? = try? ResourceLoader.loadMovieList(from: JsonFileName.movieListResponseEmpty.rawValue)
        networkManager.mockPopularMoviesResult = .success(popularMovieList ?? PopularMovieResponse())

        networkManager.getMovieList(parameters: ["page": "1"])  { (response) in
            switch response {
            case .success(let movies):
                XCTAssert(movies.popularMovies.isEmpty, "MovieList should be nil because response is empty")
            case .failure(let error):
                XCTAssert(error == nil, "Error shouldn't be nil because response is empty")
            }
        }
    }
    
    static var allTests = [
        ("testExample", testMockNetworkManagerWithCorrectData),
    ]
}
